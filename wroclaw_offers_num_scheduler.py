import time
import schedule
import datetime
import requests
import re
from os import path
from bs4 import BeautifulSoup


def log(info):
    print(info)
    with open('log.txt', 'a') as the_file:
        the_file.write(f"{datetime.date.today()}: {info}\n")


def get_offers_num_otodom(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    offers_num_list = soup.find_all("div", {"class": "css-o0w5yo e1j7rwmh6"})
    if len(offers_num_list):
        offers_num = offers_num_list[0].text.split()[-1]
        return int(offers_num)
    return None


def get_offers_num_gratka(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    offers_num_list = soup.find_all("span", {"class": "listingHeader__offersCount"})
    if len(offers_num_list):
        offers_num = offers_num_list[0].text.replace('(', '').replace(')', '').replace(' ', '')
        return int(offers_num)
    return None


def get_offers_num_morizon(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    offers_num_list = soup.find_all("span", {"class": "SvWR1M"})
    if len(offers_num_list):
        offers_num = re.sub("[^0-9]", "", offers_num_list[0].text)
        return int(offers_num)
    return None


def get_offers_num_olx(url, data_index):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    offers_num_list = soup.find_all("span", {"class": "css-wz88"})
    if len(offers_num_list) > data_index:
        return int(offers_num_list[data_index].text.replace(' ', ''))
    return None


otodom_wroclaw_urls = dict()
otodom_wroclaw_urls['wroclaw_mieszkania_sprzedaz_wtorny'] = "https://www.otodom.pl/pl/wyniki/sprzedaz/mieszkanie,rynek-wtorny/dolnoslaskie/wroclaw/wroclaw/wroclaw"
otodom_wroclaw_urls['wroclaw_mieszkania_sprzedaz_pierwotny'] = "https://www.otodom.pl/pl/wyniki/sprzedaz/mieszkanie,rynek-pierwotny/dolnoslaskie/wroclaw/wroclaw/wroclaw"
otodom_wroclaw_urls['wroclaw_mieszkania_wynajem'] = "https://www.otodom.pl/pl/wyniki/wynajem/mieszkanie/dolnoslaskie/wroclaw/wroclaw/wroclaw"
otodom_wroclaw_urls['wroclaw_pokoje_wynajem'] = "https://www.otodom.pl/pl/wyniki/wynajem/pokoj/dolnoslaskie/wroclaw/wroclaw/wroclaw"
otodom_wroclaw_urls['wroclaw_domy_sprzedaz_wtorny'] = "https://www.otodom.pl/pl/wyniki/sprzedaz/dom,rynek-wtorny/dolnoslaskie/wroclaw/wroclaw/wroclaw"
otodom_wroclaw_urls['wroclaw_domy_sprzedaz_pierwotny'] = "https://www.otodom.pl/pl/wyniki/sprzedaz/dom,rynek-pierwotny/dolnoslaskie/wroclaw/wroclaw/wroclaw"


gratka_wroclaw_urls = dict()
gratka_wroclaw_urls['wroclaw_mieszkania_sprzedaz_wtorny'] = "https://gratka.pl/nieruchomosci/mieszkania/wroclaw/wtorny"
gratka_wroclaw_urls['wroclaw_mieszkania_sprzedaz_pierwotny'] = "https://gratka.pl/nieruchomosci/mieszkania/wroclaw/pierwotny"
gratka_wroclaw_urls['wroclaw_mieszkania_wynajem'] = "https://gratka.pl/nieruchomosci/mieszkania/wroclaw/wynajem"
gratka_wroclaw_urls['wroclaw_pokoje_wynajem'] = "https://gratka.pl/nieruchomosci/pokoje/wroclaw"
gratka_wroclaw_urls['wroclaw_domy_sprzedaz_wtorny'] = "https://gratka.pl/nieruchomosci/domy/wroclaw/wtorny"
gratka_wroclaw_urls['wroclaw_domy_sprzedaz_pierwotny'] = "https://gratka.pl/nieruchomosci/domy/wroclaw/pierwotny"


morizon_wroclaw_urls = dict()
morizon_wroclaw_urls['wroclaw_mieszkania_sprzedaz_wtorny'] = "https://www.morizon.pl/mieszkania/wroclaw/?ps[market_type]=2"
morizon_wroclaw_urls['wroclaw_mieszkania_sprzedaz_pierwotny'] = "https://www.morizon.pl/mieszkania/rynek-pierwotny/wroclaw/"
morizon_wroclaw_urls['wroclaw_mieszkania_wynajem'] = "https://www.morizon.pl/do-wynajecia/mieszkania/wroclaw/"
morizon_wroclaw_urls['wroclaw_pokoje_wynajem'] = "https://www.morizon.pl/do-wynajecia/pokoje/wroclaw/"
morizon_wroclaw_urls['wroclaw_domy_sprzedaz_wtorny'] = "https://www.morizon.pl/domy/wroclaw/?ps%5Bmarket_type%5D%5B0%5D=2"
morizon_wroclaw_urls['wroclaw_domy_sprzedaz_pierwotny'] = "https://www.morizon.pl/domy/rynek-pierwotny/wroclaw/"


url_olx_wroclaw_mieszkania = "https://www.olx.pl/nieruchomosci/mieszkania/wroclaw/"
url_olx_wroclaw_domy = "https://www.olx.pl/nieruchomosci/domy/wroclaw/"
url_olx_wroclaw = "https://www.olx.pl/nieruchomosci/wroclaw/"
WYNAJEM_IDX = 0
SPRZEDAZ_IDX = 1
POKOJE_IDX = 5
olx_info = {
    'wroclaw_mieszkania_sprzedaz': {'url': url_olx_wroclaw_mieszkania, 'id': SPRZEDAZ_IDX},
    'wroclaw_mieszkania_wynajem': {'url': url_olx_wroclaw_mieszkania, 'id': WYNAJEM_IDX},
    'wroclaw_domy_sprzedaz': {'url': url_olx_wroclaw_domy, 'id': SPRZEDAZ_IDX},
    'wroclaw_domy_wynajem': {'url': url_olx_wroclaw_domy, 'id': WYNAJEM_IDX},
    'wroclaw_pokoje_wynajem': {'url': url_olx_wroclaw, 'id': POKOJE_IDX},
}


def get_offers_num_dict_otodom():
    offers_num_dict = dict()
    for name, url in otodom_wroclaw_urls.items():
        offers_num = get_offers_num_otodom(url)
        if offers_num is None:
            log("incorrect otodom: {}".format(name))
            return None
        offers_num_dict[name] = offers_num
    return offers_num_dict


def get_offers_num_dict_gratka():
    offers_num_dict = dict()
    for name, url in gratka_wroclaw_urls.items():
        offers_num = get_offers_num_gratka(url)
        if offers_num is None:
            log("incorrect gratka: {}".format(name))
            return None
        offers_num_dict[name] = offers_num
    return offers_num_dict


def get_offers_num_dict_morizon():
    offers_num_dict = dict()
    for name, url in morizon_wroclaw_urls.items():
        offers_num = get_offers_num_morizon(url)
        if offers_num is None:
            log("incorrect morizon: {}".format(name))
            return None
        offers_num_dict[name] = offers_num
    return offers_num_dict


def get_offers_num_dict_olx():
    offers_num_dict = dict()

    for name, data in olx_info.items():
        offers_num = get_offers_num_olx(data['url'], data['id'])
        if offers_num is None:
            log("incorrect olx: {}".format(name))
            return None
        offers_num_dict[name] = offers_num
    return offers_num_dict


def write_offers_num_dict_header(filename, headers):
    if path.exists(filename):
        return

    with open(filename, 'a') as the_file:
        names = list(map(lambda x: str(x), sorted(headers)))
        the_file.write(", ".join(['date'] + names) + "\n")


def write_offers_num_dict(filename, offers_num_dict):
    with open(filename, 'a') as the_file:
        numbers = list(map(lambda x: str(offers_num_dict[x]), sorted(offers_num_dict.keys())))
        the_file.write(", ".join([str(datetime.date.today())] + numbers) + "\n")


def print_offers_num_dict(filename, offers_num_dict):
    log("{} [{}]:".format(filename, datetime.date.today()))
    for name in sorted(offers_num_dict.keys()):
        log("{:38s}: {}".format(name.replace('_', ' '), offers_num_dict[name]))


def get_offers_num_job():
    offers_num_dict = get_offers_num_dict_otodom()
    if offers_num_dict:
        write_offers_num_dict('statystyka.csv', offers_num_dict)
        # print_offers_num_dict('statystyka.csv', offers_num_dict)
    offers_num_dict = get_offers_num_dict_gratka()
    if offers_num_dict:
        write_offers_num_dict('statystyka_gratka.csv', offers_num_dict)
        # print_offers_num_dict('statystyka_gratka.csv', offers_num_dict)
    offers_num_dict = get_offers_num_dict_morizon()
    if offers_num_dict:
        write_offers_num_dict('statystyka_morizon.csv', offers_num_dict)
        # print_offers_num_dict('statystyka_gratka.csv', offers_num_dict)
    offers_num_dict = get_offers_num_dict_olx()
    if offers_num_dict:
        write_offers_num_dict('statystyka_olx.csv', offers_num_dict)
        # print_offers_num_dict('statystyka_olx.csv', offers_num_dict)


def start_offers_num_scheduler():
    write_offers_num_dict_header('statystyka.csv', otodom_wroclaw_urls.keys())
    write_offers_num_dict_header('statystyka_olx.csv', olx_info.keys())
    write_offers_num_dict_header('statystyka_gratka.csv', gratka_wroclaw_urls.keys())
    write_offers_num_dict_header('statystyka_morizon.csv', morizon_wroclaw_urls.keys())
    # get_offers_num_job()
    schedule.every().day.at("11:30").do(get_offers_num_job)


start_offers_num_scheduler()
while True:
    time.sleep(schedule.idle_seconds())
    schedule.run_pending()
